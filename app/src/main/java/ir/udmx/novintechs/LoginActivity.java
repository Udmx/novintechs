package ir.udmx.novintechs;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {
    private List<LoginUserModel> listUserLogin;
    private TextInputEditText username;
    private TextInputEditText password;
    private TextInputLayout passwordLayout;
    private TextInputLayout usernameLayout;
    private RequestQueue requestQueue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        StringRequest request = new StringRequest(Request.Method.GET, "https://api.jsonbin.io/b/5ed4d7a179382f568bd0f9a2", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("TAG", "onResponse: "+response);
                listUserLogin = new ArrayList<>();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        //for add to list :)
                        LoginUserModel model = new LoginUserModel();
                        model.setName(object.getString("name"));
                        model.setPassword(object.getInt("year"));
                        listUserLogin.add(model);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("TAG", "onErrorResponse: "+error.toString());
            }
        });
        request.setTag("request");
        request.setRetryPolicy(new DefaultRetryPolicy(10000,3,2));
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(request);
        username = findViewById(R.id.et_login_username);
        password = findViewById(R.id.et_login_password);
        usernameLayout= findViewById(R.id.etl_login_username);
        passwordLayout = findViewById(R.id.etl_login_password);

        findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if(username.getText().toString().length()==0){
                    usernameLayout.setError("The amount cannot be empty");
                }
                else if(password.getText().toString().length()==0){
                    passwordLayout.setError("The amount cannot be empty");
                }
                else {
                    Log.i("TAG", "onClick: "+listUserLogin.size());
                    for (int i = 0; i <listUserLogin.size() ; i++) {
                        if(listUserLogin.get(i).getName().equals(username.getText().toString()) && String.valueOf(listUserLogin.get(i).getPassword()).equals(password.getText().toString())){
                            Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                            startActivity(intent);
                        }else {
                            Snackbar.make(v,"Enter your information correctly",BaseTransientBottomBar.LENGTH_SHORT).setAction("Retry", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //:))))
                                }
                            }).show();
                        }
                    }
                }


            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        requestQueue.cancelAll("request");
    }
}