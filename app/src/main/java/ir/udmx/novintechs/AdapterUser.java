package ir.udmx.novintechs;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

public class AdapterUser extends RecyclerView.Adapter<AdapterUser.UserViewHolder> {
    private List<UserModel> list;
    private IEventClickItemView iEventClickItemView;
    public AdapterUser(List<UserModel> list,IEventClickItemView iEventClickItemView){
        this.list = list;
        this.iEventClickItemView=iEventClickItemView;
    }
    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user , parent , false));
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class UserViewHolder extends RecyclerView.ViewHolder{
        private SimpleDraweeView avatar;
        private TextView fullName;
        private TextView email;
        public UserViewHolder(@NonNull View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.simpleDraweeView_avatar);
            fullName = itemView.findViewById(R.id.tv_fullName);
            email = itemView.findViewById(R.id.tv_email);

        }
        public void bind(final UserModel userModel){
            avatar.setImageURI(Uri.parse(userModel.getAvatar()));
            fullName.setText(""+userModel.getFirstName()+" "+userModel.getLastName());
            email.setText(userModel.getEmail());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // ای دی رو منتقل کن به اکتیویتی یا فرگمنت تا بر حسب شماره ای دی اطلاعات کاربر رو با جزییات نشون بده
                    iEventClickItemView.onClickListener(userModel.getId());
                }
            });
        }
    }
    public interface IEventClickItemView{
        void onClickListener(long id);

    }
}
